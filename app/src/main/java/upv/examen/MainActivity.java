package upv.examen;

import android.content.Intent;
import android.renderscript.Double2;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    TabHost tabH;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tabH = (TabHost)findViewById(R.id.tabHost);     //seleccionamos el tabhost
        tabH.setup();                                   // lo activamos

        TabHost.TabSpec tab1 = tabH.newTabSpec("tab1"); //aspectos de cada tab
        TabHost.TabSpec tab2 = tabH.newTabSpec("tab2");
        TabHost.TabSpec tab3 = tabH.newTabSpec("tab3");

        tab1.setIndicator("Grados");
        tab1.setContent(R.id.tab_grados);

        tab2.setIndicator("Area");
        tab2.setContent(R.id.tab_areaCirculo);

        tab3.setIndicator("Distancia");
        tab3.setContent(R.id.tab_millasKilometros);

        tabH.addTab(tab1);
        tabH.addTab(tab2);
        tabH.addTab(tab3);
    }

    public void convertirGrados(View view){
        EditText ed = (EditText)findViewById(R.id.eTxt_grados);
        Double resp = (Double.valueOf(ed.getText().toString())-32)/1.8;
        //TextView txt_resp = (TextView)findViewById(R.id.txt_respGrad);
        //txt_resp.setText(resp.toString());
        Intent intent = new Intent(this,resultados.class);
        intent.putExtra("resul",resp.toString());
        startActivity(intent);
    }

    public void obtenerArea(View view){
        EditText ed = (EditText)findViewById(R.id.eTxt_area);
        Double resp = (Double.valueOf(ed.getText().toString())*Double.valueOf(ed.getText().toString()));
        //TextView txt_resp = (TextView)findViewById(R.id.txt_respArea);
        //txt_resp.setText(resp.toString());
        Intent intent = new Intent(this,resultados.class);
        intent.putExtra("resul",resp.toString());
        startActivity(intent);
    }

    public void convertirKilometros(View view){
        EditText ed = (EditText)findViewById(R.id.eTxt_distancia);
        Double resp = (Double.valueOf(ed.getText().toString())/0.62137);
        //TextView txt_resp = (TextView)findViewById(R.id.txt_respDist);
        //txt_resp.setText(resp.toString());
        Intent intent = new Intent(this,resultados.class);
        intent.putExtra("resul",resp.toString());
        startActivity(intent);
    }
}
