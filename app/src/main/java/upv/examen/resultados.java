package upv.examen;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import org.w3c.dom.Text;

public class resultados extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_resultados);

        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        String mensaje = intent.getStringExtra("resul");

        TextView tv = new TextView(this);
        tv.setTextSize(40);
        tv.setText(mensaje);

        Log.d("imp",mensaje);
        setContentView(tv);

    }
}
